/// This pretty much is a test to incorporate both the consumer
/// and producer into a single binary by cooperating through async.
use lapin::{
    options::*,
    types::{FieldTable, ShortString},
    BasicProperties, Channel, Error, ExchangeKind,
};
use tokio::time::{sleep, Duration};
use tokio_stream::*;
use tracing::*;

use crate::common;

const EXCHANGE: &str = "hello-exchange";
const QUEUE_NAME: &str = "myqueue";
const ROUTING_KEY: &str = "my-rk";

pub async fn setup_broker(channel: &Channel) -> Result<(), Error> {
    channel
        .queue_declare(
            QUEUE_NAME,
            QueueDeclareOptions::default(),
            FieldTable::default(),
        )
        .await?;

    channel
        .exchange_declare(
            EXCHANGE,
            ExchangeKind::Direct,
            ExchangeDeclareOptions::default(),
            FieldTable::default(),
        )
        .await?;
    channel
        .queue_bind(
            QUEUE_NAME,
            EXCHANGE,
            ROUTING_KEY,
            QueueBindOptions::default(),
            FieldTable::default(),
        )
        .await?;

    info!("setup_broker");
    Ok(())
}

#[instrument(level = "info")]
pub async fn producer() -> Result<(), Error> {
    let connection = common::setup_connection_default().await?;
    let channel = connection.create_channel().await?;
    setup_broker(&channel).await?;
    sleep(Duration::from_secs(2)).await;

    // Fire off data
    let mut cmd_args: Vec<String> = std::env::args().collect();
    let message_properties = BasicProperties::default().with_content_type(ShortString::from("text/plain"));
    let message = match cmd_args.len() {
        1 => "Hello world!".into(),
        _ => cmd_args.pop().unwrap(),
    };

    info!("publish!");
    let confirm = channel
        .basic_publish(
            EXCHANGE,
            ROUTING_KEY,
            BasicPublishOptions::default(),
            message.as_bytes().to_vec(),
            message_properties,
        )
        .await?;

    debug!(?confirm);

    Ok(())
}

#[tracing::instrument(level = "info")]
pub async fn consumer() -> Result<(), Error> {
    let connection = common::setup_connection_default().await?;
    let channel = connection.create_channel().await?;
    setup_broker(&channel).await?;

    let mut consumer = channel
        .basic_consume(
            QUEUE_NAME,
            "my_consumer",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await?;

    while let Some(delivery_result) = consumer.next().await {
        let (_channel, delivery) = delivery_result.expect("error in consumer");

        let payload = String::from_utf8_lossy(&delivery.data);
        info!(message=?payload, "received message");

        delivery
            .ack(BasicAckOptions::default())
            .await
            .expect("ack failed");

        if payload == "quit" {
            break;
        }
    }

    Ok(())
}
