use lapin::Error;
use rabbit_in_action::*;

#[tokio::main]
async fn main() -> Result<(), Error> {
    telemetry::init_default();
    chapter_2::consumer().await
}
