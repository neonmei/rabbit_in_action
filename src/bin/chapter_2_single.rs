/// This pretty much is a test to incorporate both the consumer
/// and producer into a single binary by cooperating through async.
use lapin::Error;
use rabbit_in_action::*;

#[tokio::main]
async fn main() -> Result<(), Error> {
    telemetry::init_default();
    let _ = tokio::join!(chapter_2::producer(), chapter_2::consumer());
    Ok(())
}
