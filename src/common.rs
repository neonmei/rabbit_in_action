use lapin::{Connection, ConnectionProperties, Error};
use tokio_amqp::*;

// TODO: This should be expanded with AMQPS
pub async fn setup_connection_default() -> Result<Connection, Error> {
    let uri = std::env::var("AMQP_ADDR").unwrap_or_else(|_| "amqp://127.0.0.1:5672/%2f".into());
    let properties = ConnectionProperties::default().with_tokio();
    Connection::connect(&uri, properties.clone()).await
}
