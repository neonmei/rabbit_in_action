ocr := "podman"
ocn := "rabbitmq"

serve:
  {{ocr}} run --name {{ocn}} \
             --rm \
             --detach \
             --publish 127.0.0.1:5672:5672 \
             --publish 127.0.0.1:15692:15692 \
             rabbitmq:3.8-alpine
stop:
  {{ocr}} rm -f {{ocn}}

status:
  {{ocr}} exec {{ocn}} rabbitmqctl status

conn:
  {{ocr}} exec {{ocn}} rabbitmqctl list_connections

exchanges:
  {{ocr}} exec {{ocn}} rabbitmqctl list_exchanges

binds:
  {{ocr}} exec {{ocn}} rabbitmqctl list_bindings

queues:
  {{ocr}} exec {{ocn}} rabbitmqctl list_queues

cons:
  {{ocr}} exec {{ocn}} rabbitmqctl list_consumers
